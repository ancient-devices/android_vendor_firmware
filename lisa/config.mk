FIRMWARE_IMAGES := \
    abl \
    aop \
    bluetooth \
    cpucp \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    modem \
    qupfw \
    shrm \
    tz \
    uefisecapp \
    xbl \
    xbl_config

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
