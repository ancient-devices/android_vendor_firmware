FIRMWARE_IMAGES := \
    abl \
    aop \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    hyp \
    ImageFv \
    keymaster \
    modem \
    qupfw \
    storsec \
    tz \
    xbl \
    xbl_config

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
